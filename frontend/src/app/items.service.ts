import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Item } from './items/item';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {
  constructor(private http: HttpClient) { }

  api: string = 'https://localhost:7103/api/Item';

  httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  getItems() : Observable<Item[]>{
    return this.http.get<Item[]>(this.api);
  }

  addItem(newItem: any) : void{
      let json = JSON.stringify(newItem);
      this.http.post(this.api, json, this.httpOptions)
      .subscribe(Response => console.log(Response));
  }

  editItem(item: any) : void {
    let json = JSON.stringify(item);
    this.http.patch(this.api, json, this.httpOptions)
    .subscribe(Response => console.log(Response));
  }

  
  deleteItem(id: number) : void {
    this.http.delete(this.api + '/' + id, this.httpOptions)
    .subscribe(Response => console.log(Response));
  }
}
