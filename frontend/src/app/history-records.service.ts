import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HistoryRecord } from './history-records/history-record';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HistoryRecordsService {
  constructor(private http: HttpClient) { }
  api: string = 'https://localhost:7103/api/History';

  httpOptions: any = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  getHistory() : Observable<HistoryRecord[]>{
    return this.http.get<HistoryRecord[]>(this.api);
  }

  addRecord(newRecord: any){
    let json = JSON.stringify(newRecord);
     this.http.post(this.api, json, this.httpOptions)
     .subscribe();
  }

  editRecord(record: any){
    let json = JSON.stringify(record);
     this.http.patch(this.api, json, this.httpOptions)
     .subscribe();
  }

  deleteRecord(id: number){
     this.http.delete(this.api + '/' + id, this.httpOptions)
     .subscribe();
  }
}
