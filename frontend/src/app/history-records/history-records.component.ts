import { Component, OnInit } from '@angular/core';
import { HistoryRecord } from './history-record';
import { HistoryRecordsService } from '../history-records.service';

@Component({
  selector: 'app-history-records',
  templateUrl: './history-records.component.html',
  styleUrls: ['./history-records.component.css']
})
export class HistoryRecordsComponent implements OnInit {
  constructor(private historyService: HistoryRecordsService) { }

  historyRecords?: HistoryRecord[];

  ngOnInit(): void {
      this.historyService.getHistory().subscribe(Response => this.historyRecords = Response);
  }

  add(data: any){
    const newRecord = {
      id: data.id,
      surnameOfUser: data.surnameOfUser,
      usedItemId: data.usedItemId,
      yearOfStart: data.yearOfStart,
      yearOfEnd: data.yearOfEnd,
    };

    this.historyService.addRecord(data);
    location.reload();
  }

  edit(data: any){
    this.historyService.editRecord(data);
    location.reload();
  }

  delete(data: any){
    this.historyService.deleteRecord(data.id);
    location.reload();
  }
}
