export class HistoryRecord {
    public id?: number;
    public surnameOfUser?: string;
    public usedItemId?: number;
    public yearOfStart?: number;
    public yearOfEnd?: number;
}