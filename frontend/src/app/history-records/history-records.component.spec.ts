import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryRecordsComponent } from './history-records.component';

describe('HistoryRecordsComponent', () => {
  let component: HistoryRecordsComponent;
  let fixture: ComponentFixture<HistoryRecordsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryRecordsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HistoryRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
