import { TestBed } from '@angular/core/testing';

import { HistoryRecordsService } from './history-records.service';

describe('HistoryRecordsService', () => {
  let service: HistoryRecordsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HistoryRecordsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
