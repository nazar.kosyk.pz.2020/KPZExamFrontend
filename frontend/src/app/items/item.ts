export class Item {
    public id?: number;
    public name?: string;
    public price?: number;
    public yearOfRelease?: number;
    public roomId?: number;
    public dateOfWriteOff?: string;
}