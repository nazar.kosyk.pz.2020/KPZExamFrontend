import { Component, OnInit } from '@angular/core';
import { Item } from './item';
import { ItemsService } from '../items.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  constructor(private itemService: ItemsService) { }
  items?: Item[];

  
  ngOnInit(): void {
    this.itemService.getItems().subscribe(Response => this.items = Response);
  }

  add(data: any){
    const newItem = {
      id: data.id,
      name: data.name,
      price: data.price,
      yearOfRelease: data.yearOfRelease,
      roomId: data.roomId,
      dateOfWriteOff: data.dateOfWriteOff
    };

    this.itemService.addItem(newItem);
    location.reload();
  }

  edit(data: any){
    this.itemService.editItem(data);
    location.reload();
  }

  delete(data: any){
    this.itemService.deleteItem(data.id);
    location.reload();
  }
}
